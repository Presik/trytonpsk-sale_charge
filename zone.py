# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool


class CityZone(ModelSQL, ModelView):
    "City Zone"
    __name__ = "country.city.zone"
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    city_code = fields.Many2One('party.city_code', 'City', states={
        'invisible': Bool(Eval('parent')),
    })
    neighbourhoods = fields.One2Many('country.city.neighbourhood', 'zone',
        'Neighbourhoods')


class Neighbourhood(ModelSQL, ModelView):
    "Neighbourhood"
    __name__ = "country.city.neighbourhood"
    name = fields.Char('Name', required=True)
    code = fields.Char('Code', required=True)
    zone = fields.Many2One('country.city.zone', 'Zone')

    def get_rec_name(self, name=None):
        if self.zone:
            name = f'[{self.zone.name}] {self.name}'
        return name
