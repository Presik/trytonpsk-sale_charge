# This file is part of Tryton.  The COPYRIGHT file at the top level
# of this repository contains the full copyright notices and license terms.
from trytond.model import Workflow, ModelView, ModelSQL, fields
from trytond.wizard import Wizard, StateView, Button, StateTransition
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.modules.company import CompanyReport
from trytond.pool import Pool
from decimal import Decimal
from trytond.i18n import gettext
from .exceptions import PaymentTermMissingError
from trytond.exceptions import UserError

STATES = {
    'readonly': (Eval('state') != 'draft'),
}


class Charge(Workflow, ModelSQL, ModelView):
    "Sale Charge"
    __name__ = 'sale.charge'
    party = fields.Many2One('party.party', 'Party', select=True, states=STATES)
    price_list = fields.Many2One('product.price_list', 'Price List',
        domain=[('company', '=', Eval('company'))], states=STATES)
    location = fields.Many2One('stock.location', 'Location', states=STATES,
        required=False, domain=[('type', '=', 'warehouse')])
    date = fields.Date('Date', required=True, states=STATES)
    lines = fields.One2Many('sale.charge.line', 'charge', 'Lines',
        states=STATES)
    company = fields.Many2One('company.company', 'Company', required=True,
        states=STATES)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('confirm', 'Confirm'),
        ('cancel', 'Canceled'),
        ], 'State', readonly=True, select=True)
    sale_rebate = fields.Numeric('Rebate', digits=(16, 2), states=STATES,
        help='Rebate in percentage')
    neighbourhoods = fields.Function(fields.One2Many(
        'country.city.neighbourhood',
        None, 'Neighbourhoods', states=STATES), 'get_neighbourhoods')
    sales = fields.Function(fields.One2Many('sale.sale', None, 'Sales',
        states=STATES), 'get_sales')
    return_sales = fields.One2Many('sale.charge.sale_return', 'charge',
        'Return Sales', states=STATES)
    products = fields.Function(fields.Dict(None, 'Products'), 'get_products')
    total_charge = fields.Function(fields.Numeric('Total Charge',
        digits=(16, 2)), 'get_total_amount')
    vehicle_plate = fields.Char('Vehicle Plate', states=STATES)
    main_driver = fields.Char('Main Driver', states=STATES)
    aux_driver = fields.Char('Auxiliar Driver', states=STATES)
    second_aux = fields.Char('Second Auxiliar', states=STATES)
    orders_quantity = fields.Function(fields.Integer('Orders Qty'),
        'get_orders_quantity')
    dispatched_by = fields.Char('Dispatched By', states=STATES)
    amount_delivered_rate = fields.Function(fields.Numeric(
        'Amount Delivered Rate', digits=(16, 2)), 'get_amount_delivered_rate')
    freight_value = fields.Numeric('Freight Value', digits=(16, 2),
        states=STATES)
    statements = fields.Function(fields.Many2Many('account.statement',
        None, None, 'Statements'), 'get_statements')
    packages_quantity = fields.Function(fields.Integer('Packages Quantity'),
        'get_packages_quantity')
    comment = fields.Text('Comment')
    number = fields.Char('Number', states={'readonly': True})

    @classmethod
    def __setup__(cls):
        super(Charge, cls).__setup__()
        cls._order.insert(0, ('date', 'DESC'))
        cls._transitions |= set((
            ('draft', 'confirm'),
            ('draft', 'cancel'),
        ))
        cls._buttons.update({
            'cancel': {
                'invisible': Eval('state') != 'draft'
            },
            'draft': {
                'invisible': Eval('state') != 'confirm',
            },
            'confirm': {
                'invisible': Eval('state') != 'draft',
            },
        })

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_company():
        return Transaction().context.get('company') or False

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, charges):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancel')
    def cancel(cls, charges):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('confirm')
    def confirm(cls, charges):
        for charge in charges:
            charge.set_number()
        #     cls._create_sale(charge)
        pass

    def set_number(self):
        configuration = Pool().get('sale.configuration')(1)
        if configuration.charge_sequence and not self.number:
            self.number = configuration.charge_sequence.get()
            self.save()
        else:
            raise UserError(
                gettext('sale_charge.msg_charge_sequence_missing'))

    @classmethod
    def _create_sale(cls, charge):
        Party = Pool().get('party.party')
        PaymentTerm = Pool().get('account.invoice.payment_term')
        SaleLine = Pool().get('sale.line')
        Sale = Pool().get('sale.sale')
        Product = Pool().get('product.product')

        payment = PaymentTerm.search([])
        if not payment:
            raise PaymentTermMissingError(
                gettext('sale_charge.msg_payment_term_missing'))
        if not charge.price_list:
            price_list = None
        else:
            price_list = charge.price_list.id
        sale, = Sale.create([{
            'company': charge.company.id,
            'payment_term': payment[0].id,
            'party': charge.party.id,
            'price_list': price_list,
            'sale_date': charge.date,
            'warehouse': charge.location.id,
            'state': 'draft',
            'invoice_address': Party.address_get(charge.party, type='invoice'),
            'shipment_address': Party.address_get(charge.party, type='delivery'),
            'description': ('Planilla'),
        }])

        for charge_line in charge.lines:
            if charge_line.settle <= 0:
                continue

            line = SaleLine()
            line.unit = charge_line.product.sale_uom
            line.sale = sale.id
            with Transaction().set_context(line._get_context_sale_price()):
                unit_price = Product.get_sale_price([charge_line.product],
                        charge_line.settle or 0)[charge_line.product.id]
                unit_price = Decimal(str(round(unit_price, 2)))
            SaleLine.create([{
                'sale': sale.id,
                'type': 'line',
                'unit': charge_line.product.default_uom.id,
                'quantity': charge_line.settle,
                'unit_price': unit_price,
                'product': charge_line.product.id,
                'taxes': [('add', charge_line.product.customer_taxes_used)],
                'description': charge_line.product.name,
            }])

    def get_packages_quantity(self, name=None):
        res = []
        for line in self.lines:
            res.append(line.request)
        return int(sum(res))

    def get_statements(self, name=None):
        statements = [p.statement for s in self.sales for p in s.payments]
        return set(statements)

    def get_neighbourhoods(self, name=None):
        nbhds = []
        for line in self.lines:
            for sl in line.sale_lines:
                if sl.neighbourhood and sl.neighbourhood.id not in nbhds:
                    nbhds.append(sl.neighbourhood.id)
            return nbhds

    def get_orders_quantity(self, name=None):
        if self.sales:
            return len(self.sales)
        return 0

    def get_sales(self, name=None):
        if self.lines:
            sales = []
            for line in self.lines:
                for sl in line.sale_lines:
                    if sl.sale.id not in sales:
                        sales.append(sl.sale.id)
            return sales

    def get_products(self, name=None):
        products = {}
        for line in self.lines:
            if line.id not in products.keys():
                products[line.id] = {
                    'product': None,
                    'amount': 0,
                    'request': 0,
                    'sell_off': 0,
                    'product_returned': 0,
                    'charge': None,
                    'sale_line': None,
                }
            products[line.id]['product'] = line.product.id,
            products[line.id]['amount'] = line.amount,
            products[line.id]['request'] = line.request,
        return products.values()

    def get_amount_delivered_rate(self, name=None):
        total_paid_sales = 0
        total_sales = 0
        rate = 0
        for sale in self.sales:
            total_paid_sales += sale.paid_amount
            total_sales = sale.total_amount
        for sale_ in self.return_sales:
            if sale_.sale_return:
                total_paid_sales += sale_.sale_return.total_amount
        if total_sales:
            rate = (total_paid_sales / total_sales) * 100
        return Decimal(str(round(rate, 2)))

    def get_total_amount(self, name=None):
        if self.lines:
            return sum([line.amount for line in self.lines])

    @classmethod
    def import_data(cls, fields_names, data):
        pool = Pool()
        StatementLine = pool.get('account.statement.line')
        Production = pool.get('production')
        cursor = Transaction().connection.cursor()

        lines = StatementLine.search([
            ('description', '=', 'Pago Multiple'),
            ('amount', '<', 0),
            ('move', '!=', None),
        ])
        for l in lines:
            move = l.move
            move = l.move
            movelines = move.lines
            for ml in movelines:
                if ml.account.id == 1037 and ml.debit != 0:
                    cursor.execute(
                        "UPDATE account_move_line set debit=credit, credit=debit where id=%s" % ml.id
                    )
                if ml.account.id == 1039 and ml.credit != 0:
                    cursor.execute(
                        "UPDATE account_move_line set debit=credit, credit=debit where id=%s" % ml.id
                    )

        productions = Production.search([])
        for p in productions:
            move_process = p.raw_production_move
            move_finish = p.production_finished_move
            if not move_process or not move_finish:
                continue
            value = 0
            for l in move_process.lines:
                if l.account.id == 1045:
                    value = l.debit
            if value:
                for l in move_finish.lines:
                    if l.account.id == 1045:
                        cursor.execute("UPDATE account_move_line \
                            set credit=%s where id=%s" % (value, l.id))
                    else:
                       cursor.execute("UPDATE account_move_line \
                            set debit=%s where id=%s" % (value, l.id))
        return 0


class ChargeLine(ModelSQL, ModelView):
    "Sale Charge Line"
    __name__ = 'sale.charge.line'
    product = fields.Many2One('product.product', 'Product', required=True,
            domain=[('salable', '=', 'True')])
    uom = fields.Function(fields.Many2One('product.uom', 'UOM'), 'get_uom')
    unit_digits = fields.Function(fields.Integer('Unit Digits'),
            'get_unit_digits')
    request = fields.Float('Request', digits=(16, Eval('unit_digits', 0)),
            depends=['unit_digits'])
    sell_off = fields.Float('Sell Off', digits=(16, 0))
    product_returned = fields.Float('Returned', digits=(16, 0))
    settle = fields.Function(fields.Float('Settle', digits=(16, 0),
            readonly=True), 'get_settle')
    charge = fields.Many2One('sale.charge', 'Charge', select=True,
            ondelete='CASCADE')
    sale_lines = fields.One2Many('sale.charge.line.sale_line', 'charge_line', 'Sale Lines')
    amount = fields.Numeric('Amount', digits=(16, 2))
    neighbourhoods = fields.Function(fields.Char('Neighbourhood'), 'get_neighbourhoods')

    @classmethod
    def __setup__(cls):
        super(ChargeLine, cls).__setup__()
        cls._order.insert(0, ('product', 'ASC'))

    @staticmethod
    def default_unit_digits():
        return 2

    @staticmethod
    def default_request():
        return 0

    @staticmethod
    def default_sell_off():
        return 0

    @staticmethod
    def default_product_returned():
        return 0

    @fields.depends('product', 'uom', 'unit_digits')
    def on_change_product(self):
        if self.product:
            self.uom = self.product.default_uom.id
            self.unit_digits = self.product.default_uom.digits
            self.unit_digits = 2

    def get_uom(self, name):
        return self.product.default_uom.id

    def get_unit_digits(self, name):
        return self.product.default_uom.digits

    def get_settle(self, name):
        sell_off = self.sell_off or 0
        request = self.request or 0
        returned = self.product_returned or 0
        return ((sell_off + request) - returned)

    def get_neighbourhoods(self, name=None):
        if self.sale_lines:
            neighbourhoods = []
            for line in self.sale_lines:
                if not line.neighbourhood:
                    continue
                name = line.neighbourhood.name
                if name not in neighbourhoods:
                    neighbourhoods.append(name)
            return ' - '.join(neighbourhoods)


class ChargeLineSaleLine(ModelSQL, ModelView):
    "Sale Charge Line - Sale Line"
    __name__ = 'sale.charge.line.sale_line'
    charge_line = fields.Many2One('sale.charge.line', 'Charge Line', select=True,
            ondelete='CASCADE')
    sale_line = fields.Many2One('sale.line', 'Sale Line', readonly=True)
    neighbourhood = fields.Function(fields.Many2One('country.city.neighbourhood',
        'Neighbourhood', readonly=True), 'get_data')
    party = fields.Function(fields.Many2One('party.party', 'Client',
        readonly=True), 'get_data')
    sale = fields.Function(fields.Many2One('sale.sale', 'Sale', readonly=True),
        'get_data')

    def get_data(self, name):
        if self.sale_line:
            if name == 'neighbourhood' and self.sale_line.sale.neighbourhood:
                return self.sale_line.sale.neighbourhood.id
            if name == 'party' and self.sale_line.sale.party:
                return self.sale_line.sale.party.id
            if name == 'sale':
                return self.sale_line.sale.id


class ChargeSaleReturn(ModelSQL, ModelView):
    "Charge Sale Return"
    __name__ = 'sale.charge.sale_return'
    charge = fields.Many2One('sale.charge', 'Charge', required=True)
    sale = fields.Many2One('sale.sale', 'Sale', required=True)
    kind = fields.Many2One('sale.charge.sale_return.kind', 'Kind', required=True)
    sale_return = fields.Many2One('sale.sale', 'Sale Return')
    # return_amount = fields.Function(fields.Numeric('Amount', digits=(16, 2)),
    #     'on_change_with_return_amount')
    notes = fields.Text('Notes')

    @classmethod
    def __setup__(cls):
        super(ChargeSaleReturn, cls).__setup__()
        cls._buttons.update({
            'manage_sale_return': {
                'invisible': Eval('sale_return'),
            },
        })

    @classmethod
    @ModelView.button
    def manage_sale_return(cls, records):
        for rec in records:
            if not rec.sale:
                return
            rec.manage_return()

    def manage_return(self):
        Sale = Pool().get('sale.sale')
        return_sale, = Sale.copy([self.sale])
        return_sale.origin = self.sale
        for line in return_sale.lines:
            if line.type == 'line':
                line.quantity *= -1
            return_sale.lines = return_sale.lines  # Force saving
        Sale.save([return_sale])
        self.sale_return = return_sale.id
        self.save()

    @fields.depends('sale_return')
    def on_change_with_return_amount(self, name=None):
        if self.sale_return:
            self.sale_return.total_amount


class ChargeSaleReturnKind(ModelSQL, ModelView):
    "Charge Sale Return Kind"
    __name__ = 'sale.charge.sale_return.kind'
    name = fields.Char('Name', required=True)


class ChargeReport(CompanyReport):
    __name__ = 'sale.charge'


class ChargeReportSummary(CompanyReport):
    __name__ = 'sale.charge_summary'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        products = {}
        date_list = []
        for charge in records:
            for line in charge.lines:
                if line.product.id in products.keys():
                    products[line.product.id]['quantity'] += line.settle
                else:
                    products[line.product.id] = {
                            'code': line.product.code,
                            'product': line.product.name,
                            'uom': line.product.default_uom.rec_name,
                            'quantity': line.settle,
                            }
            date_list.append(charge.date)
        report_context['products'] = products.values()
        report_context['date_start'] = min(date_list)
        report_context['date_end'] = max(date_list)

        return report_context


class CreateChargeFromSaleStart(ModelView):
    'Create Charge From Sale Start'
    __name__ = 'sale_charge.create_charge_from_sale.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    date = fields.Date('Date', required=True)
    neighbourhoods = fields.Many2Many('country.city.neighbourhood', None, None,
        'Neighbourhood', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_date():
        Date = Pool().get('ir.date')
        return Date.today()


class CreateChargeFromSale(Wizard):
    'Create Charge From Sale'
    __name__ = 'sale_charge.create_charge_from_sale'
    start = StateView(
        'sale_charge.create_charge_from_sale.start',
        'sale_charge.create_charge_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Create', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        Charge = pool.get('sale.charge')
        ChargeLine = pool.get('sale.charge.line')

        neighbourhood_ids = [neg.id for neg in self.start.neighbourhoods]
        sales = Sale.search([
            ('state', 'in', ['processing']),
            ('shipment_date', '=', self.start.date),
            ('company', '=', self.start.company.id),
            ('neighbourhood', 'in', neighbourhood_ids),
        ], order=[('neighbourhood', 'ASC'), ('party.id', 'ASC')])

        create_ = {
            'party': None,
            'price_list': None,
            'date': self.start.date,
            'company': self.start.company.id,
            'state': 'draft',
        }
        charge, = Charge.create([create_])
        create_lines = {}
        for sale in sales:
            if sale.total_amount <= 0:
                continue
            for line in sale.lines:
                amount_ = line.quantity * int(line.unit_price_w_tax)
                if line.product.id not in create_lines.keys():
                    create_lines[line.product.id] = {
                        'product': None,
                        'request': 0,
                        'amount': 0,
                        'charge': None,
                        'sale_lines': [('create', [])],
                    }
                create_lines[line.product.id]['product'] = line.product.id
                create_lines[line.product.id]['charge'] = charge.id
                create_lines[line.product.id]['request'] += line.quantity
                create_lines[line.product.id]['amount'] += round(amount_, 2)
                create_lines[line.product.id]['sale_lines'][0][1].append({
                    'sale_line': line.id,
                })
        Sale.write(sales, {'charge': charge.id})
        ChargeLine.create(create_lines.values())
        return 'end'


class PaymentSalesStart(ModelView):
    'Payment Sales Start'
    __name__ = 'sale_charge.payment_sales.start'
    statement = fields.Many2One('account.statement', 'Statement',
        required=True, domain=[('state', '=', 'draft')])
    sales = fields.Many2Many('sale.sale', None, None, 'Sales')
    total_amount = fields.Function(fields.Numeric('Total Amount',
        digits=(16, 2), depends=['sales']), 'on_change_with_total_amount')

    @staticmethod
    def default_sales():
        charge_id = Transaction().context.get('active_id')
        charge = Charge(charge_id)
        sales_ids = [sale.id for sale in charge.sales]
        return sales_ids

    @fields.depends('sales')
    def on_change_with_total_amount(self, name=None):
        res = 0
        if self.sales:
            res = sum([s.total_amount for s in self.sales])
        return res


class PaymentSales(Wizard):
    'Payment Sales'
    __name__ = 'sale_charge.payment_sales'
    start = StateView(
        'sale_charge.payment_sales.start',
        'sale_charge.sale_charge_payment_sales_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'pay_', 'tryton-ok', default=True),
        ])
    pay_ = StateTransition()

    def transition_pay_(self):
        Statement = Pool().get('account.statement')
        args = {
            'sale_ids': [s.id for s in self.start.sales],
            'statement_id': self.start.statement.id,
        }
        Statement.multipayment_invoices(args)
        return 'end'
