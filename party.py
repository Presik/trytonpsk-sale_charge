# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'
    zone = fields.Many2One('country.city.zone', 'Zone')
    neighbourhood = fields.Many2One('country.city.neighbourhood',
        'Neighbourhood', domain=[
            ('zone', '=', Eval('zone'))
        ])
    agent = fields.Many2One('commission.agent', 'Agent')


class PartyDunningReport(metaclass=PoolMeta):
    __name__ = 'invoice_report.party_dunning'

    @classmethod
    def get_invoices(cls, party):
        Invoice = Pool().get('account.invoice')
        invoices = Invoice.search([
            ['OR',
                ('agent.party', '=', party.id),
                ('party', '=', party.id),
            ], [
                ('state', 'in', ['posted', 'validated']),
                ('type', '=', 'out'),
            ]], order=[
                ('reference', 'ASC'),
                ('invoice_date', 'ASC')
            ])
        return invoices
