# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import logging
from decimal import Decimal
from datetime import date, timedelta

from trytond.model import fields, ModelView, ModelSQL
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.wizard import (
    Wizard, StateView, StateReport, Button, StateTransition)
from trytond.report import Report
from trytond.i18n import gettext
from .exceptions import (
    SaleDeviceInvalid, StatementDraftError, PartyAccountError)

logger = logging.getLogger(__name__)


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'
    neighbourhood = fields.Many2One('country.city.neighbourhood', 'Neighbourhood')
    charge = fields.Many2One('sale.charge', 'Charge')

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls.price_list.states['readonly'] = (Eval('state') != 'draft')

    @fields.depends('neighbourhood', 'party')
    def on_change_party(self):
        super(Sale, self).on_change_party()
        if self.party and self.party.neighbourhood:
            self.neighbourhood = self.party.neighbourhood.id
        if self.party and self.party.agent:
            self.agent = self.party.agent.id
        if self.party and self.party.addresses:
            self.shipment_address = self.party.addresses[0].id
            self.invoice_address = self.party.addresses[0].id

    @fields.depends('shipment_date', 'invoice_date')
    def on_change_shipment_date(self):
        if self.shipment_date:
            self.invoice_date = self.shipment_date

    @fields.depends('shipment_address', 'invoice_address')
    def on_change_invoice_address(self):
        # super(Sale, self).on_change_shipment_address()
        if self.invoice_address:
            self.shipment_address = self.invoice_address

    @classmethod
    def create(cls, vlist):
        sales = super(Sale, cls).create(vlist)
        for sale in sales:
            if sale.party and sale.party.neighbourhood:
                cls.write([sale], {
                    'neighbourhood': sale.party.neighbourhood.id
                })
            if sale.shipment_address and not sale.invoice_address:
                cls.write([sale], {
                    'invoice_address': sale.shipment_address.id
                })
        return sales


class SaleProductNeighbourhoodStart(ModelView):
    'Sale Product Neighbourhood Start'
    __name__ = 'sale_charge.sale_product_neighbourhood.start'
    shipment_date = fields.Date('Shipment Date', required=True)
    company = fields.Many2One('company.company', 'Company', required=True)
    product = fields.Many2One('product.product', 'Product')
    neighbourhoods = fields.Many2Many('country.city.neighbourhood',
        None, None, 'Neighbourhood')
    consolidated = fields.Boolean('Consolidated')
    only_draft = fields.Boolean('Only Draft')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class SaleProductNeighbourhood(Wizard):
    'Sale Product Neighbourhood'
    __name__ = 'sale_charge.sale_product_neighbourhood'
    start = StateView(
        'sale_charge.sale_product_neighbourhood.start',
        'sale_charge.sale_product_neighbourhood_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
        ])
    print_ = StateReport('sale_charge.report_sale_product_neighbourhood')

    def list_dates(self):
        date_ = self.start.shipment_date
        data = {
            'date1': date_,
            'date2': date_ + timedelta(days=1),
            'date3': date_ + timedelta(days=2),
            'date4': date_ + timedelta(days=3),
            'date5': date_ + timedelta(days=4),
            'date6': date_ + timedelta(days=5),
            'date7': date_ + timedelta(days=6),
        }
        return data

    def do_print_(self, action):
        product_id, neighbourhood_ids = None, []
        if self.start.product:
            product_id = self.start.product.id
        if self.start.neighbourhoods:
            neighbourhood_ids = [z.id for z in self.start.neighbourhoods]
        data = {
            'company': self.start.company.id,
            'shipment_date': self.start.shipment_date,
            'product': product_id,
            'neighbourhoods': neighbourhood_ids,
            'consolidated': self.start.consolidated,
            'only_draft': self.start.only_draft
            # 'dates' : self.list_dates()
        }
        return action, data

    def transition_print_(self):
        return 'end'


class SaleProductNeighbourhoodReport(Report):
    __name__ = 'sale_charge.report_sale_product_neighbourhood'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()

        Company = pool.get('company.company')
        SaleLine = pool.get('sale.line')
        # neighbourhood_ids = []

        dom = [
            ('sale.company', '=', data['company']),
            ('sale.shipment_date', '=', data['shipment_date']),
            # ('sale.shipment_date', '<=', end_date),
        ]
        if data['only_draft']:
            dom.append(
                ('sale.state', '=', 'draft'),
            )
        else:
            dom.append(
                ('sale.state', '!=', 'draft'),
            )

        if data['product']:
            dom.append(
                ('product', '=', data['product']),
            )
        if data['neighbourhoods']:
            dom.append(
                ('sale.party.neighbourhood', 'in', data['neighbourhoods']),
            )

        lines = SaleLine.search([dom])
        neighbourhoods = {}
        total_amount = []

        if data['consolidated']:
            class Neighbourhood_():
                pass
            neighbourhood_ = Neighbourhood_()
            neighbourhood_.id = 0
            neighbourhood_.name = 'all'

        for line in lines:
            if data['consolidated']:
                neighbourhood = neighbourhood_
            else:
                neighbourhood = line.sale.party.neighbourhood
            if not neighbourhood:
                continue
            total_amount.append(line.quantity * float(line.unit_price_w_tax))

            if neighbourhood.id not in neighbourhoods.keys():
                neighbourhoods[neighbourhood.id] = {
                    'name': neighbourhood.name,
                    'products': {},
                }

            if line.product.id not in neighbourhoods[neighbourhood.id]['products'].keys():
                neighbourhoods[neighbourhood.id]['products'][line.product.id] = {
                    'code': line.product.code or '',
                    'name': line.product.name,
                    'quantity': [],
                    'amount': [],
                }

            neighbourhoods[neighbourhood.id]['products'][line.product.id]['quantity'].append(line.quantity)
            neighbourhoods[neighbourhood.id]['products'][line.product.id]['amount'].append(line.quantity * float(line.unit_price_w_tax))

            # if line.product.id  not in zones[line.sale.zone.id].keys():
            #     zones[line.sale.zone.id][line.product.id] = {
            #         'product': line.product.name,
            #         'zone': line.sale.zone.name,
            #         'dates': cls.create_dates_dict(),
            #     }
            # for date_ in data['dates'].values():
            #     if line.sale.shipment_date == date_:
            #         zones[line.sale.zone.id][line.product.id]['dates'][date_] += line.quantity

        report_context['records'] = neighbourhoods.values()
        report_context['total_amount'] = sum(total_amount)
        report_context['shipment_date'] = data['shipment_date']
        report_context['company'] = Company(data['company'])
        return report_context

    @classmethod
    def create_dates_dict(cls):
        date_dict = {
            data['dates']['date1']: 0,
            data['dates']['date2']: 0,
            data['dates']['date3']: 0,
            data['dates']['date4']: 0,
            data['dates']['date5']: 0,
            data['dates']['date6']: 0,
            data['dates']['date7']: 0,
        }
        return date_dict


class PayMultiSalesForm(ModelView):
    'Pay Multi Sales Form'
    __name__ = 'sale_charge.pay_multisales_form'
    journal = fields.Many2One('account.statement.journal', 'Statement Journal',
        domain=[
            ('id', 'in', Eval('journals', [])),
        ], depends=['journals'], required=True)
    journals = fields.One2Many('account.statement.journal', None,
        'Allowed Statement Journals')


class PayMultiSales(Wizard):
    'Pay Multi Sales'
    __name__ = 'sale_charge.pay_multisales'
    start = StateView(
        'sale_charge.pay_multisales_form',
        'sale_charge.pay_multisales_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Pay', 'pay_', 'tryton-ok', default=True),
        ])
    pay_ = StateTransition()

    @classmethod
    def __setup__(cls):
        super(PayMultiSales, cls).__setup__()

    def default_start(self, fields):
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        sale_device = user.sale_device or False
        if user.id != 0 and not sale_device:
            raise SaleDeviceInvalid(
                gettext('sale_charge.msg_not_sale_device'))
        vals = {
            'journal': sale_device.journal.id if sale_device.journal else None,
            'journals': [j.id for j in sale_device.journals],
        }
        return vals

    def transition_pay_(self):
        pool = Pool()
        User = pool.get('res.user')
        user = User(Transaction().user)
        Sale = pool.get('sale.sale')
        Statement = pool.get('account.statement')
        StatementLine = pool.get('account.statement.line')
        ids = Transaction().context['active_ids']
        if not ids:
            return 'end'

        sales = Sale.browse(ids)
        form = self.start
        for sale in sales:
            device_id = user.sale_device.id if user.sale_device else sale.sale_device.id
            statements = Statement.search([
                    ('journal', '=', form.journal),
                    ('state', '=', 'draft'),
                    ('sale_device', '=', device_id),
                ], order=[('date', 'DESC')])
            if not statements:
                raise StatementDraftError(gettext(
                    'sale_charge.msg_not_draft_statement', s=form.journal.name)
                )

            if not sale.number:
                Sale.set_number([sale])

            if not sale.party.account_receivable:
                raise PartyAccountError(gettext(
                    'sale_charge.msg_party_without_account_receivable',
                    s=sale.party.name))

            account = sale.party.account_receivable.id
            desc = sale.invoices[0].number + ' [ ' + sale.number + ' ]' if sale.invoices else sale.invoice_number
            payment = StatementLine(
                statement=statements[0].id,
                date=date.today(),
                amount=sale.residual_amount,
                party=sale.party.id,
                account=account,
                description=desc,
                sale=sale.id,
            )
            payment.save()

            if sale.total_amount != sale.paid_amount:
                return 'start'
            sale.save()
            for inv in sale.invoices:
                if inv.state == 'posted':
                    inv.write([inv], {'state': 'draft'})
            Sale.workflow_to_end([sale])
        return 'end'


class PriceList(metaclass=PoolMeta):
    __name__ = 'product.price_list'
    percent_commission = fields.Numeric('percent_commission', digits=(2, 2))


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'
    commission_amount = fields.Function(fields.Numeric('Commission Amount'),
        'get_commission_amount')

    def get_commission_amount(self, name=None):
        if self.sale.price_list and self.sale.price_list.percent_commission:
            if self.unit_price and self.product and self.quantity:
                return round(
                    (((self.unit_price) * int(self.quantity)) * self.sale.price_list.percent_commission), 0
                )
        return 0

    # @classmethod
    # def write(cls, records, values):
    #     super(SaleLine, cls).write(records, values)
    #     for line in records:
    #         if line.quantity < 0 and line.commission_amount > 0:
    #             line.commission_amount = line.commission_amount * -1
    #             line.save()

    @classmethod
    def create(cls, vlist):
        lines = super(SaleLine, cls).create(vlist)
        for line in lines:
            if not line.commission_amount:
                if line.unit_price and line.product and line.quantity and line.sale.price_list:
                    percent_commission = line.sale.price_list.percent_commission
                    if percent_commission:
                        line.commission_amount = round((((line.unit_price) * int(line.quantity)) * percent_commission), 0)
            if line.quantity < 0 and line.commission_amount > 0:
                line.commission_amount = line.commission_amount * -1
            line.save()
        return lines


class MoneyCount(metaclass=PoolMeta):
    __name__ = 'sale_pos.money_count'
    description = fields.Char('Description')


class Statement(metaclass=PoolMeta):
    __name__ = 'account.statement'
    expense_amounts = fields.One2Many('sale_charge.expense_amounts',
        'statement', 'Expense Amounts')
    total_expense = fields.Function(fields.Numeric("Total Expense"),
        'get_total_expense')

    def get_total_expense(self, name):
        res = 0
        for line in self.expense_amounts:
            res = res + line.amount
        return res

    def get_amount_difference(self, name):
        res = super(Statement, self).get_amount_difference(name)
        if not res:
            res = 0
        return res + self.total_expense


class ExpenseAmounts(ModelSQL, ModelView):
    'Expense Amounts'
    __name__ = 'sale_charge.expense_amounts'
    statement = fields.Many2One('account.statement', 'Statement', required=True)
    bill = fields.Integer('Bill', required=True)
    quantity = fields.Integer('Quantity')
    amount = fields.Function(fields.Numeric('Amount', digits=(16, 2)),
        'get_amount')
    description = fields.Char('Description')

    @staticmethod
    def default_quantity():
        return 0

    def get_amount(self, name=None):
        res = Decimal(0)
        if self.quantity and self.bill:
            res = Decimal(self.quantity * self.bill)
        return res
