# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval


class ProductTemplate(metaclass=PoolMeta):
    __name__ = 'product.template'
    salable_web = fields.Boolean('Salable Web', states={
            'readonly': ~Eval('active', True),
            }, depends=['active'])
