# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from decimal import Decimal

_ZERO = Decimal('0.00')


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'
    commission_amount = fields.Function(fields.Numeric('Commission Amount'),
        'get_commission_amount')
    unit_price_w_tax = fields.Function(fields.Numeric('Unit Price with Tax'),
        'get_price_with_tax')
    amount_w_tax = fields.Function(fields.Numeric('Amount with Tax'),
        'get_price_with_tax')

    def get_commission_amount(self, name=None):
        value_ = 0
        if not self.invoice or self.invoice.type == 'in':
            return
        if self.invoice.type == 'out' and self.unit_price and self.product \
            and self.quantity and self.origin and hasattr(self.origin, 'sale'):
                percent_commission = self.origin.sale.price_list.percent_commission
                if percent_commission:
                    value_ = round((((self.unit_price) * int(self.quantity)) * percent_commission), 0)
        return value_

    @classmethod
    def create(cls, vlist):
        lines = super(InvoiceLine, cls).create(vlist)
        # PLEASE FIXME
        # NotImplementedError: Falta la función de escritura para el campo "Valor de la Comisión" en "Línea de factura".
        # for line in lines:
        #     if line.invoice.type == 'out' and line.origin and hasattr(line.origin, 'sale'):
        #         cls.write([line], {'commission_amount': line.origin.commission_amount})
        #         line.invoice.write([line.invoice], {'invoice_date': line.origin.sale.shipment_date})
        return lines

    @classmethod
    def get_price_with_tax(cls, lines, names):
        pool = Pool()
        Tax = pool.get('account.tax')
        Company = pool.get('company.company')
        amount_w_tax = {}
        unit_price_w_tax = {}

        def compute_amount_with_tax(line):
            tax_amount = _ZERO
            amount = _ZERO

            if line.taxes:
                tax_list = Tax.compute(line.taxes, line.unit_price or _ZERO,
                    line.quantity or 0.0)

                tax_amount = sum([t['amount'] for t in tax_list], _ZERO)

            if line.unit_price:
                amount = line.unit_price * Decimal(line.quantity)
            return amount + tax_amount

        for line in lines:
            amount = _ZERO
            unit_price = _ZERO
            if line.invoice:
                currency = line.invoice.currency
            else:
                # company = Company(Transaction().context.get('company'))
                company = Company(1)
                currency = company.currency

            if line.type == 'line' and line.quantity:
                amount = compute_amount_with_tax(line)
                unit_price = amount / Decimal(str(line.quantity))

            # Only compute subtotals if the two fields are provided to speed up
            elif line.type == 'subtotal' and len(names) == 2:
                for line2 in line.invoice.lines:
                    if line2.type == 'line':
                        amount2 = compute_amount_with_tax(line2)
                        if currency:
                            amount2 = currency.round(amount2)
                        amount += amount2
                    elif line2.type == 'subtotal':
                        if line == line2:
                            break
                        amount = _ZERO

            if currency:
                amount = currency.round(amount)

            amount_w_tax[line.id] = amount
            unit_price_w_tax[line.id] = unit_price

        result = {
            'amount_w_tax': amount_w_tax,
            'unit_price_w_tax': unit_price_w_tax,
        }
        for key in result.keys():
            if key not in names:
                del result[key]
        return result
