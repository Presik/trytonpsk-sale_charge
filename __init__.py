# This file is part of Tryton.  The COPYRIGHT file at the top level of this
# repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import product
from . import charge
from . import party
from . import zone
from . import agent
from . import sale
from . import invoice
from . import configuration


def register():
    Pool.register(
        # product.ProductTemplate,
        charge.Charge,
        charge.ChargeLine,
        charge.ChargeLineSaleLine,
        party.Party,
        zone.CityZone,
        zone.Neighbourhood,
        agent.AgentNeighbourhood,
        agent.Agent,
        sale.PriceList,
        sale.Sale,
        sale.SaleProductNeighbourhoodStart,
        sale.PayMultiSalesForm,
        sale.Statement,
        sale.MoneyCount,
        sale.ExpenseAmounts,
        sale.SaleLine,
        charge.CreateChargeFromSaleStart,
        charge.PaymentSalesStart,
        charge.ChargeSaleReturnKind,
        charge.ChargeSaleReturn,
        invoice.InvoiceLine,
        configuration.Configuration,
        module='sale_charge', type_='model')
    Pool.register(
        charge.ChargeReport,
        charge.ChargeReportSummary,
        party.PartyDunningReport,
        sale.SaleProductNeighbourhoodReport,
        module='sale_charge', type_='report')
    Pool.register(
        sale.SaleProductNeighbourhood,
        sale.PayMultiSales,
        charge.PaymentSales,
        charge.CreateChargeFromSale,
        module='sale_charge', type_='wizard')
